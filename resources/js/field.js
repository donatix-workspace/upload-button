Nova.booting((Vue, router) => {
    Vue.component("nova-button", require("./components/NovaButton.vue").default);
    Vue.component("base-modal", require("./components/base/Modal.vue").default);
    Vue.component(
        "index-nova-button",
        require("./components/IndexField.vue").default
    );
    Vue.component(
        "detail-nova-button",
        require("./components/DetailField.vue").default
    );
});
